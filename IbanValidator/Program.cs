﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IBAN_validate
{
    class Program
    {
        static void Main(string[] args)
        {
            string IBAN = "BG80 BNBG 9661 1020 3456 78";

            Console.Write(ValidIban(IBAN));
            Console.ReadLine();

        }

        public static bool ValidIban(string iban)
        {
            if (Regex.IsMatch(iban, @"^[BG]{2}"))
            {
                string formating = iban.Replace(" ", "");
                char[] subArray = formating.ToArray();


                char[] firstElement = new char[4];
                for (int i = 0; i < 4; i++)
                    firstElement[i] = subArray[i];
                char[] SecondElement = new char[22];
                for (int i = 4; i < 22; i++)
                    SecondElement[i] = subArray[i];
                string[] iban_change = new string[22];
                for (int i = 0; i < 18; i++)
                    iban_change[i] = SecondElement[i + 4].ToString();
                for (int i = 18; i < 22; i++)
                    iban_change[i] = firstElement[i - 18].ToString();
                char[] abc = new char[36];

                for (int i = 0; i < 22; i++)
                {
                    switch (iban_change[i])
                    {
                        case "A":
                            iban_change[i] = (string)"10";
                            break;
                        case "B":
                            iban_change[i] = (string)"11";
                            break;
                        case "C":
                            iban_change[i] = (string)"12";
                            break;
                        case "D":
                            iban_change[i] = (string)"13";
                            break;
                        case "E":
                            iban_change[i] = (string)"14";
                            break;
                        case "F":
                            iban_change[i] = (string)"15";
                            break;
                        case "G":
                            iban_change[i] = (string)"16";
                            break;
                        case "H":
                            iban_change[i] = (string)"17";
                            break;
                        case "I":
                            iban_change[i] = (string)"18";
                            break;
                        case "J":
                            iban_change[i] = (string)"19";
                            break;
                        case "K":
                            iban_change[i] = (string)"20";
                            break;
                        case "L":
                            iban_change[i] = (string)"21";
                            break;
                        case "M":
                            iban_change[i] = (string)"22";
                            break;
                        case "N":
                            iban_change[i] = (string)"23";
                            break;
                        case "O":
                            iban_change[i] = (string)"24";
                            break;
                        case "P":
                            iban_change[i] = (string)"25";
                            break;
                        case "Q":
                            iban_change[i] = (string)"26";
                            break;
                        case "R":
                            iban_change[i] = (string)"27";
                            break;
                        case "S":
                            iban_change[i] = (string)"28";
                            break;
                        case "T":
                            iban_change[i] = (string)"29";
                            break;
                        case "U":
                            iban_change[i] = (string)"30";
                            break;
                        case "V":
                            iban_change[i] = (string)"31";
                            break;
                        case "W":
                            iban_change[i] = (string)"32";
                            break;
                        case "X":
                            iban_change[i] = (string)"33";
                            break;
                        case "Y":
                            iban_change[i] = (string)"34";
                            break;
                        case "Z":
                            iban_change[i] = (string)"35";
                            break;

                    }
                }
                string substring = "";
                for (int i = 0; i < 22; i++)
                    substring = substring + iban_change[i];
                decimal substringConvert = Convert.ToDecimal(substring);
                decimal Module = substringConvert % 97;
                if (Module == 1) return true;
                else return false;
            }
            else
            {
                return false;
            }
        }
    }

}